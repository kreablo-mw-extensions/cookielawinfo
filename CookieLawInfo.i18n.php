<?php
/**
 * Internationalisation file for extension CookieLawInfo
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
    'cookielawinfo' => 'Cookie Law Info',
    'cookielawinfo-desc' => 'Present a cookie policy before enabling cookies as required by European law.',
    'cookielawinfo-default-policy-message' => 'This site will use cookies for managing the user session.  You will not be able to login unless you accept the cookie policy.',
    'cookielawinfo-read-policy' => 'Read cookie policy',
    'cookielawinfo-accept-policy' => 'Accept cookie policy',
    'cookielawinfo-accept' => 'Accept',
    'cookielawinfo-cookie-policy' => 'Cookie policy',
);
