<?php
/**
 * Copyright (C) 2014 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

class CookieLawInfoHooks
{

    private static $ID = 'cookie-law-accepted-cookie-policy';

    private static $hasAcceptedPolicy = false;

    private static $haveCheckedCookie = false;

    private static function serializeUrl( $p ) {
        $url = '';

        if (isset($p[PHP_URL_SCHEME])) {
            $url = $p[PHP_URL_SCHEME] . '://';
        }

        if (isset($p[PHP_URL_HOST])) {
            if (isset($p[PHP_URL_USER])) {
                $url .= $p[PHP_URL_USER];
                if (isset($p[PHP_URL_PASS])) {
                    $url .= ':' . $p[PHP_URL_PASS];
                }
                $url .= '@';
            }

            $url .= $p[PHP_URL_HOST];

            if (isset($p[PHP_URL_PORT])) {
                $url .= ':' . $p[PHP_URL_PORT];
            }
        }

        if (isset($p[PHP_URL_PATH])) {
            $url .= $p[PHP_URL_PATH];
        }

        if (isset($p[PHP_URL_QUERY])) {
            $url .= '?' . $p[PHP_URL_QUERY];
        }

        if (isset($p[PHP_URL_FRAGMENT])) {
            $url .= '#' . $p[PHP_URL_FRAGMENT];
        }

        return $url;

    }

    public static function onSiteNoticeAfter( &$sitenotice, Skin $skin )  {

        if ( !self::$haveCheckedCookie ) {
            self::checkAccepted();
        }

        if ( !self::$hasAcceptedPolicy ) {

			/*
            session_unset();

            // unset cookies
            if (isset($_SERVER['HTTP_COOKIE'])) {
                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
                foreach($cookies as $cookie) {
                    $parts = explode('=', $cookie);
                    $name = trim($parts[0]);
                    setcookie($name, '', time() - 1000);
                    setcookie($name, '', time() - 1000, '/');
                }
            }
			*/

            $url = parse_url($skin->getContext()->getRequest()->getRequestURL());
            if (isset( $url[PHP_URL_QUERY] )) {
                $url[PHP_URL_QUERY] .= '&' . self::$ID . '=true';
            } else {
                $url[PHP_URL_QUERY] = self::$ID . '=true';
            }
            global $wgSitename;
            if ($wgSitename == 'Akvopedia') {
                $readUrl = Title::newFromText('Akvopedia:Privacy policy');
            } else {
                $readUrl = Title::newFromText('akvopedia:Akvopedia:Privacy policy');
            }
            $readUrl = $readUrl->getLocalUrl();

            $msg = '<div id="cookie-policy"><div id="cookie-policy-message"><div id="cookie-policy-message-p">' . self::getPolicyMessage( $skin ) . '</div></div>' .
                '<div id="cookie-policy-read-link"><p><a href="' . $readUrl . '"><strong>' . $skin->msg('cookielawinfo-read-policy')->text() . '</strong></a></p></div>' .
                '<div id="cookie-policy-accept-link"><p><a href="' . self::serializeUrl( $url ) . '"><strong>' . $skin->msg('cookielawinfo-accept-policy')->text() . '</strong></a></p></div></div>';

            $sitenotice = $msg  . $sitenotice;

			$out = $skin->getOutput();

            $out->addModules( 'ext.CookieLawInfo' );

        }

        return true;
    }

    public static function onWebResponseSetCookie( &$name, &$value, &$expire, $options ) {
		/*
        if ( !self::$haveCheckedCookie ) {
            self::checkAccepted();
        }

        return self::$hasAcceptedPolicy;
		*/
		return true;
    }

    public static function onSkinTemplateOutputPageBeforeExec(  &$skin, &$template ) {
        $p = $template->get( 'personal_urls' );

        if ( !self::$hasAcceptedPolicy ) {

            if (isset($p['login'])) {
                $p['login']['class'] = 'cookie-policy-not-accepted';
            }
            if (isset($p['createaccount'])) {
                $p['createaccount']['class'] = 'cookie-policy-not-accepted';
            }
            if (isset($p['anonlogin'])) {
                $p['anonlogin']['class'] = 'cookie-policy-not-accepted';
            }

            $template->set( 'personal_urls', $p );

        }


        return true;
    }

    private static function checkAccepted() {
        global $wgRequest, $wgUser;

        self::$haveCheckedCookie = true;

		if ( !is_null($wgUser) && $wgUser->isAllowed( 'bot' ) ) {
			self::$hasAcceptedPolicy = true;
			return;
		}

        $cookie = $wgRequest->getCookie( self::$ID );

        self::$hasAcceptedPolicy = strcasecmp($cookie, 'true' ) == 0;

        if ( !self::$hasAcceptedPolicy ) {
            $q = $wgRequest->getQueryValues();

            self::$hasAcceptedPolicy = isset($q[self::$ID]) && strcasecmp($q[self::$ID], 'true') == 0;

            if ( self::$hasAcceptedPolicy ) {
                $wgRequest->response()->setCookie( self::$ID, 'true' );
            }
        }
    }

    private static function getPolicyMessage( $skin ) {
        $title = Title::newFromText( 'cookie-policy-message', NS_MEDIAWIKI );
        $page = WikiPage::factory( $title );

        if ( $page->exists() ) {
            return $page->getContent( Revision::FOR_THIS_USER )->getParserOutput( $title )->getText();
        } else {
            return '<p>' . $skin->msg( 'cookielawinfo-default-policy-message' )->text() . '</p>';
        }
    }

    public static function onResourceLoaderGetConfigVars( array &$vars ) {
        global $wgCookieDomain, $wgCookiePrefix, $wgOut;

        $vars['wgCookieDomain'] = $wgCookieDomain;

		$settings = json_encode( array(
                'cookie-name' => $wgCookiePrefix . self::$ID,
                'accept-label' => $wgOut->msg('cookielawinfo-accept')->text(),
                'cookie-policy-label' => $wgOut->msg('cookielawinfo-cookie-policy')->text()
            ) );

		$vars['wgCookieLawInfoSettings'] = $settings;

        return true;
    }

}