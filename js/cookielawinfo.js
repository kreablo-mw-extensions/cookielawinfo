
;(function (mw, document, $) {
	$(document).ready( function() {
		var p = $('#cookie-policy');

        p.hide();
		show_cookie_policy();

	} );

	function show_cookie_policy() {

		var s = JSON.parse(mw.config.get('wgCookieLawInfoSettings'));

		var msg = $('#cookie-policy-message');
		msg.detach();

		var bar = $('<div id="cookie-policy-bar"></div>');
		$('body').append(msg);
                msg.wrap(bar);

		var accept = $('<a id="cookie-policy-accept-button">' + s['accept-label'] + '</a>');
		var read   = $('<a id="cookie-policy-read-button">' + s['cookie-policy-label'] + '</a>');

		var readlink=$('#cookie-policy-read-link').find('a').get(0).href;
		read.get(0).href = readlink;

		p = $('#cookie-policy-message-p p');

		p.append(accept);
		p.append(read);

		var a = p.find('a');

		accept.click(function() {
			$('#cookie-policy-bar').hide();
			$('#p-personal').find('.cookie-policy-not-accepted').removeClass('cookie-policy-not-accepted');

			$.cookie( s['cookie-name'], 'true', {path: '/', domain: mw.config.get('wgCookieDomain'), expires: 365  } );
		});
	}
})(mw, document, jQuery);